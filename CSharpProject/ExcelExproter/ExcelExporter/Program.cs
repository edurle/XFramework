﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using OfficeOpenXml;

namespace XFramework
{
    public struct SheetInfo
    {
        /// <summary>
        /// 概括
        /// </summary>
        public string summary;
        /// <summary>
        /// 属性类型
        /// </summary>
        public string property;
        /// <summary>
        /// 字段名
        /// </summary>
        public string fieldName;

        public SheetInfo(string summary, string property, string fieldName)
        {
            this.summary = summary;
            this.property = property;
            this.fieldName = fieldName;
        }
    }

    class Program
    {
        const string rootPath = "../../../../../../";
        const string excelPath = rootPath + "Excel";
        const string outputConfigPath = rootPath + "UnityProject/Assets/Resources/Config/Gen";
        const string unityOutputClassPath = rootPath + "UnityProject/Assets/Scripts/XFramework/Runtime/Module/Config/Gen";
        const string thisOutputClassPath = "../../../Config";
        const string jsonPath = rootPath + "CSharpProject/Json";
        /// <summary>
        /// 数组的分隔符
        /// </summary>
        const string delimiter = ";";
        static string file = "";

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                string arg = args[0];
                if (arg == "a")
                {
                    if (Run())
                        Console.WriteLine("生成配置类成功");
                    else
                        Console.WriteLine("生成配置类失败");
                }
                else if (arg == "b")
                {
                    ExportExcelProtobuf();
                }
            }
            //Console.Read();
        }

        static bool Run()
        {     
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            if (!Directory.Exists(excelPath))
            {
                Console.WriteLine("Excel目录不存在");
                return false;
            }

            try
            {
                foreach (string path in Directory.GetFiles(excelPath))
                {
                    string fileName = Path.GetFileName(path);
                    if (!fileName.EndsWith(".xlsx") || fileName.StartsWith("~$"))
                    {
                        continue;
                    }
                    using Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    using ExcelPackage ep = new ExcelPackage(stream);
                    string name = Path.GetFileNameWithoutExtension(path);
                    //Console.WriteLine($"fileName = {fileName}, name = {name}");

                    ExportBySheet(ep, name);
                }

                file = "";

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine(file);
                Console.Read();
                return false;
            }            
        }

        static string GetRootPath()
        {
            string folderName = "XFramework";
            string curPath = Environment.CurrentDirectory;
            int index = curPath.IndexOf(folderName);
            return curPath.Substring(0, index + folderName.Length);
        }

        /// <summary>
        /// 通过工作表导出成类
        /// </summary>
        /// <param name="ep"></param>
        /// <param name="name"></param>
        static void ExportBySheet(ExcelPackage ep, string name)
        {
            HashSet<string> fieldType = new HashSet<string>();
            List<SheetInfo> sheetInfos = new List<SheetInfo>();

            const int row = 2;
            foreach (ExcelWorksheet sheet in ep.Workbook.Worksheets)
            {
                fieldType.Clear();
                sheetInfos.Clear();

                string sheetName = sheet.Name;
                if (sheetName.StartsWith("#") || sheetName.Contains("Sheet"))
                    continue;

                for (int col = 1; col <= sheet.Dimension.End.Column; col++)
                {
                    string header = sheet.Cells[row, col].Text.Trim();
                    if (header.StartsWith("#"))
                        continue;

                    string fieldName = sheet.Cells[row + 2, col].Text.Trim();
                    if (!fieldType.Add(fieldName))
                        continue;

                    string summary = sheet.Cells[row + 1, col].Text;
                    string fieldProperty = sheet.Cells[row + 3, col].Text.Trim();

                    SheetInfo sheetInfo = new SheetInfo(summary, fieldProperty, fieldName);
                    sheetInfos.Add(sheetInfo);
                }

                file = sheetName;
                string configName = FormatSheetName(sheetName);
                ExportJson(sheet, sheetInfos, configName);
                ExportClass(sheetInfos, configName);
            }
        }

        /// <summary>
        /// 格式化表名
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        static string FormatSheetName(string name)
        {
            if (name.EndsWith("config", true, null))
                name = name.Substring(0, name.Length - 6);

            name += "Config";
            return name;
        }

        /// <summary>
        /// 导出成Class
        /// </summary>
        /// <param name="sheetInfos"></param>
        /// <param name="className"></param>
        static void ExportClass(List<SheetInfo> sheetInfos, string className)
        {
            string header = @"using System.Collections.Generic;
using ProtoBuf;
using Newtonsoft.Json;

namespace XFramework
{";
            string footer = "}";
            string managerContent = $@"
    [ProtoContract]
    [Config]
    public partial class {className}Manager : ProtoObject
    {{
        public static {className}Manager Instance {{ get; private set; }}

        [ProtoIgnore]
        private Dictionary<int, {className}> dictConfigs = new Dictionary<int, {className}>();

        [ProtoMember(1)]
        [JsonProperty]
        private List<{className}> list = new List<{className}>();

        public {className}Manager()
        {{
            Instance = this;
        }}

        [ProtoAfterDeserialization]
        public void AfterDeserialization()
        {{
            foreach ({className} config in list)
            {{
                dictConfigs.Add(config.Id, config);
            }}
            this.EndInit();
        }}

        public int GetConfigsCount()
        {{
            return dictConfigs.Count;
        }}

        public Dictionary<int, {className}> GetAll()
        {{
            return dictConfigs;
        }}

        public {className} Get(int id)
        {{
            if (dictConfigs.TryGetValue(id, out var config))
                return config;

            throw new System.Exception({"$"}""{{nameof({className})}} 配置为 {{id}} 不存在"");
        }}

        public bool Contain(int id)
        {{
            return dictConfigs.ContainsKey(id);
        }}
    }}";

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < sheetInfos.Count; i++)
            {
                int count = i + 1;
                var info = sheetInfos[i];
                sb.AppendLine($@"       /// <summary>");
                sb.AppendLine($@"       /// {info.summary}");
                sb.AppendLine($@"       /// </summary>");
                sb.AppendLine(@$"       [ProtoMember({count}, IsRequired = true)]");
                sb.AppendLine(@$"       public {info.property} {info.fieldName} {{get; set;}}");
            }

            string classContent = @$"
    [ProtoContract]
    public partial class {className}
    {{
{sb.ToString()}
    }}";

            string contents = $"{header}\n{managerContent}\n{classContent}\n{footer}";
            if (!Directory.Exists(unityOutputClassPath))
                Directory.CreateDirectory(unityOutputClassPath);

            if (!Directory.Exists(thisOutputClassPath))
                Directory.CreateDirectory(thisOutputClassPath);

            string filePath = $"{unityOutputClassPath}/{className}.cs";
            File.WriteAllText(filePath, contents);
            filePath = $"{thisOutputClassPath}/{className}.cs";
            File.WriteAllText(filePath, contents);
        }

        /// <summary>
        /// 导出成Json
        /// </summary>
        /// <param name="sheetInfos"></param>
        /// <param name="jsonName"></param>
        static void ExportJson(ExcelWorksheet sheet, List<SheetInfo> sheetInfos, string jsonName)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("{\"list\":[");
            for (int row = 6; row <= sheet.Dimension.End.Row; row++)
            {
                sb.Append("{");
                for (int col = 1; col <= sheet.Dimension.End.Column; col++)
                {
                    var info = sheetInfos[col - 1];
                    string value = sheet.Cells[row, col].Text.Trim();
                    string content = $"\"{info.fieldName}\":{GetPropertyValue(info.property, value)}";
                    if (col == 1)
                        sb.Append(content);
                    else
                        sb.Append($",{content}");
                }
                sb.AppendLine("},");
            }
            sb.AppendLine("]}");

            if (!Directory.Exists(jsonPath))
                Directory.CreateDirectory(jsonPath);

            string filePath = $"{jsonPath}/{jsonName}.txt";
            File.WriteAllText(filePath, sb.ToString());
        }

        /// <summary>
        /// 通过数据类型返回json的值
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        static string GetPropertyValue(string property, string value)
        {
            StringBuilder sb = new StringBuilder();
            switch (property)
            {
                case "bool":
                    if (string.IsNullOrEmpty(value))
                        return "false";
                    else
                    {
                        string str = value.ToLower();
                        if (str == "false" || str == "true")
                            return str;
                        throw new Exception($"无法识别的bool值：{value}");
                    }
                case "int":
                case "long":
                    return string.IsNullOrEmpty(value) ? "0" : value;
                case "float":
                case "double":
                    return string.IsNullOrEmpty(value) ? "0.0" : value;
                case "float[]":
                case "double[]":
                case "int[]":
                case "long[]":
                    if (string.IsNullOrEmpty(value))
                        return "null";
                    else
                    {
                        string[] str = value.Split(delimiter);
                        sb.Clear();
                        sb.Append("[");
                        for (int i = 0; i < str.Length; i++)
                        {
                            if (i == 0)
                                sb.Append(str[i]);
                            else
                                sb.Append($",{str[i]}");

                        }
                        sb.Append("]");
                        return sb.ToString();
                    }
                case "string":
                    return $"\"{value}\"";
                case "string[]":
                    if (string.IsNullOrEmpty(value))
                        return "null";
                    else
                    {
                        string[] str = value.Split(delimiter);
                        sb.Clear();
                        sb.Append("[");
                        for (int i = 0; i < str.Length; i++)
                        {
                            if (i == 0)
                                sb.Append($"\"{str[i]}\"");
                            else
                                sb.Append($",\"{str[i]}\"");
                        }
                        sb.Append("]");
                        return sb.ToString();
                    }
                default:
                    throw new Exception($"无法识别的类型：{property}");
            }
        }

        static void ExportExcelProtobuf()
        {
            List<string> classNameList = new List<string>();
            string classPath = thisOutputClassPath;

            if (!Directory.Exists(outputConfigPath))
            {
                Directory.CreateDirectory(outputConfigPath);
            }
            foreach (string classFile in Directory.GetFiles(classPath, "*.cs"))
            {
                classNameList.Add(Path.GetFileNameWithoutExtension(classFile));
            }

            foreach (var className in classNameList)
            {
                Type managerType = Type.GetType($"XFramework.{className}Manager");
                Type configType = Type.GetType($"XFramework.{className}");

                Console.WriteLine(managerType);
                Console.WriteLine(configType);

                ProtoBuf.Serializer.NonGeneric.PrepareSerializer(managerType);
                ProtoBuf.Serializer.NonGeneric.PrepareSerializer(configType);

                try
                {
                    string json = File.ReadAllText(Path.Combine(jsonPath, $"{className}.txt"));
                    object configObject = Newtonsoft.Json.JsonConvert.DeserializeObject(json, managerType);

                    string path = Path.Combine(outputConfigPath, $"{className}Manager.bytes");
                    using FileStream file = File.Create(path);
                    ProtoBuf.Serializer.Serialize(file, configObject);
                }
                catch (Exception e)
                {
                    throw new Exception($"请检查{className}配置 -> {e}");
                }
            }
        }
    }
}
