# 进阶版Unity C#框架

#### 介绍
master分支(视频版本)远远不够用，于是有了这个版本。
建立了一个Q群755562062，欢迎进群交流。

#### 软件架构
面向对象编程


#### 安装教程

无需安装，下载即用

#### 使用说明

入口场景为Init，入口类为Init.cs

#### 功能如下：

1.	资源加载，本框架目前只支持Resources目录加载资源，但是预留了接口可以使用其他插件<br>
2.	场景加载，本框架目前只支持添加到Build Settings里的场景，但是预留了接口可以使用其他插件<br>
3.	支持Excel配置表导出到Unity和C#，Tools目录里有导表须知，拉取工程后请执行Tools/BuildExcel.bat，然后执行Tools/A_ToJsonAndClass.bat<br>
4.	资源绑定以及自动回收，GameObject对象池和类对象池<br>
5.	全面UI管理，方便好用<br>
6.	事件系统, 接口为IEvent<T><br>
7.	敬请期待


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
