using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace XFramework
{
	[UIEvent(UIType.UISetting)]
    internal sealed class UISettingEvent : AUIEvent
    {
        public override string Key => UIPathSet.UISetting;

        public override bool IsFromPool => true;

        public override UI OnCreate()
        {
            return UI.Create<UISetting>();
        }
    }

    public partial class UISetting : UI, IAwake
	{	
		public void Initialize()
		{
            this.Publish(new UIEventType.OnOpenSetting());
            this.GetButton(KExit)?.AddClickListener(this.Close);
		}

        public override void OnCancel()
        {
            this.Close();
        }

        protected override void OnClose()
		{
			
		}
	}
}
