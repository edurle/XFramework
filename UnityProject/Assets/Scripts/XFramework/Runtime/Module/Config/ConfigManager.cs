using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace XFramework
{
    public sealed class ConfigManager : CommonObject
    {
        /// <summary>
        /// 配置文件名 -> 二进制内容
        /// </summary>
        private Dictionary<string, byte[]> configBytes = new Dictionary<string, byte[]>();

        /// <summary>
        /// 配置类型 -> 反序列化出来的对象
        /// </summary>
        private Dictionary<Type, object> configProtos = new Dictionary<Type, object>();

        /// <summary>
        /// 标记了Config特性的配置类
        /// <para>类名 -> 类型</para>
        /// </summary>
        private Dictionary<string, Type> configTypes = new Dictionary<string, Type>();

        protected override void Init()
        {
            var types = TypesManager.Instance.GetTypes(typeof(ConfigAttribute));
            foreach (var type in types)
            {
                configTypes.Add(type.Name, type);
            }
        }

        /// <summary>
        /// 加载所有配置
        /// </summary>
        /// <returns></returns>
        public async Task LoadAllConfigs()
        {
            async Task Load(string fileName)
            {
                TextAsset asset = await ResourcesManager.LoadAssetAsync<TextAsset>(this, $"Config/Gen/{fileName}");
                if (asset is null)
                    return;

                configBytes[fileName] = asset.bytes;
                //ResourcesManager.ReleaseAsset(asset);
            }

            using var tasks = XList<Task>.Create();
            foreach (var fileName in configTypes.Keys)
            {
                tasks.Add(Load(fileName));
            }

            await Task.WhenAll(tasks);
        }

        /// <summary>
        /// 反序列化所有的配置
        /// </summary>
        /// <returns></returns>
        public async Task DeserializeConfigs(List<Task> tasks)
        {
            if (configTypes.Count == configProtos.Count)
                return;

            if (configBytes.Count == 0)
                await this.LoadAllConfigs();

            if (configBytes.Count == 0)
                return;

            foreach (var configInfo in configTypes)
            {
                string name = configInfo.Key;
                Type configType = configInfo.Value;
                if (configBytes.TryGetValue(name, out var bytes))
                {
                    if (configProtos.ContainsKey(configType))
                        continue;

                    tasks.Add(Deserialize(configType, bytes));
                }
                else
                {
                    Log.Error($"配置加载失败，名为{name}，请检查配置文件");
                }
            }
        }

        /// <summary>
        /// 反序列化配置
        /// </summary>
        /// <param name="configType"></param>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private async Task Deserialize(Type configType, byte[] bytes)
        {
            var task = Task.Run(() =>
            {
                object obj = ProtobufHelper.FromBytes(bytes, configType);
                return obj;
            });

            object configObj = await task;
            configProtos.Add(configType, configObj);
        }

        protected override void Destroy()
        {
            configBytes.Clear();
            configProtos.Clear();
            configTypes.Clear();
        }
    }
}
