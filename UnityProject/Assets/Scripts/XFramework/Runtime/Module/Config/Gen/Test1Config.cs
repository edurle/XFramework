using System.Collections.Generic;
using ProtoBuf;
using Newtonsoft.Json;

namespace XFramework
{

    [ProtoContract]
    [Config]
    public partial class Test1ConfigManager : ProtoObject
    {
        public static Test1ConfigManager Instance { get; private set; }

        [ProtoIgnore]
        private Dictionary<int, Test1Config> dictConfigs = new Dictionary<int, Test1Config>();

        [ProtoMember(1)]
        [JsonProperty]
        private List<Test1Config> list = new List<Test1Config>();

        public Test1ConfigManager()
        {
            Instance = this;
        }

        [ProtoAfterDeserialization]
        public void AfterDeserialization()
        {
            foreach (Test1Config config in list)
            {
                dictConfigs.Add(config.Id, config);
            }
            this.EndInit();
        }

        public int GetConfigsCount()
        {
            return dictConfigs.Count;
        }

        public Dictionary<int, Test1Config> GetAll()
        {
            return dictConfigs;
        }

        public Test1Config Get(int id)
        {
            if (dictConfigs.TryGetValue(id, out var config))
                return config;

            throw new System.Exception($"{nameof(Test1Config)} 配置为 {id} 不存在");
        }

        public bool Contain(int id)
        {
            return dictConfigs.ContainsKey(id);
        }
    }

    [ProtoContract]
    public partial class Test1Config
    {
       /// <summary>
       /// ID
       /// </summary>
       [ProtoMember(1, IsRequired = true)]
       public int Id {get; set;}
       /// <summary>
       /// 速度
       /// </summary>
       [ProtoMember(2, IsRequired = true)]
       public float Speed {get; set;}

    }
}