using System.Collections.Generic;
using ProtoBuf;
using Newtonsoft.Json;

namespace XFramework
{

    [ProtoContract]
    [Config]
    public partial class TestConfigManager : ProtoObject
    {
        public static TestConfigManager Instance { get; private set; }

        [ProtoIgnore]
        private Dictionary<int, TestConfig> dictConfigs = new Dictionary<int, TestConfig>();

        [ProtoMember(1)]
        [JsonProperty]
        private List<TestConfig> list = new List<TestConfig>();

        public TestConfigManager()
        {
            Instance = this;
        }

        [ProtoAfterDeserialization]
        public void AfterDeserialization()
        {
            foreach (TestConfig config in list)
            {
                dictConfigs.Add(config.Id, config);
            }
            this.EndInit();
        }

        public int GetConfigsCount()
        {
            return dictConfigs.Count;
        }

        public Dictionary<int, TestConfig> GetAll()
        {
            return dictConfigs;
        }

        public TestConfig Get(int id)
        {
            if (dictConfigs.TryGetValue(id, out var config))
                return config;

            throw new System.Exception($"{nameof(TestConfig)} 配置为 {id} 不存在");
        }

        public bool Contain(int id)
        {
            return dictConfigs.ContainsKey(id);
        }
    }

    [ProtoContract]
    public partial class TestConfig
    {
       /// <summary>
       /// ID
       /// </summary>
       [ProtoMember(1, IsRequired = true)]
       public int Id {get; set;}
       /// <summary>
       /// 姓名
       /// </summary>
       [ProtoMember(2, IsRequired = true)]
       public string Name {get; set;}
       /// <summary>
       /// 城市
       /// </summary>
       [ProtoMember(3, IsRequired = true)]
       public string[] City {get; set;}
       /// <summary>
       /// 身高
       /// </summary>
       [ProtoMember(4, IsRequired = true)]
       public float[] Height {get; set;}

    }
}