using System.Collections.Generic;
using ProtoBuf;
using Newtonsoft.Json;

namespace XFramework
{

    [ProtoContract]
    [Config]
    public partial class HRDConfigManager : ProtoObject
    {
        public static HRDConfigManager Instance { get; private set; }

        [ProtoIgnore]
        private Dictionary<int, HRDConfig> dictConfigs = new Dictionary<int, HRDConfig>();

        [ProtoMember(1)]
        [JsonProperty]
        private List<HRDConfig> list = new List<HRDConfig>();

        public HRDConfigManager()
        {
            Instance = this;
        }

        [ProtoAfterDeserialization]
        public void AfterDeserialization()
        {
            foreach (HRDConfig config in list)
            {
                dictConfigs.Add(config.Id, config);
            }
            this.EndInit();
        }

        public int GetConfigsCount()
        {
            return dictConfigs.Count;
        }

        public Dictionary<int, HRDConfig> GetAll()
        {
            return dictConfigs;
        }

        public HRDConfig Get(int id)
        {
            if (dictConfigs.TryGetValue(id, out var config))
                return config;

            throw new System.Exception($"{nameof(HRDConfig)} 配置为 {id} 不存在");
        }

        public bool Contain(int id)
        {
            return dictConfigs.ContainsKey(id);
        }
    }

    [ProtoContract]
    public partial class HRDConfig
    {
       /// <summary>
       /// 
       /// </summary>
       [ProtoMember(1, IsRequired = true)]
       public int Id {get; set;}
       /// <summary>
       /// X坐标
       /// </summary>
       [ProtoMember(2, IsRequired = true)]
       public int X {get; set;}
       /// <summary>
       /// Y坐标
       /// </summary>
       [ProtoMember(3, IsRequired = true)]
       public int Y {get; set;}
       /// <summary>
       /// 高度
       /// </summary>
       [ProtoMember(4, IsRequired = true)]
       public int H {get; set;}
       /// <summary>
       /// 宽度
       /// </summary>
       [ProtoMember(5, IsRequired = true)]
       public int W {get; set;}
       /// <summary>
       /// 资源路径
       /// </summary>
       [ProtoMember(6, IsRequired = true)]
       public string Res {get; set;}

    }
}