using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace XFramework
{
    public class Game : Singleton<Game>, IDisposable
    {
        public void Start()
        {
            Log.ILog = new UnityLogger();

            AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
            {
                Log.Error($"{e}");
            };

            SynchronizationContext.SetSynchronizationContext(ThreadSynchronizationContext.Instance);

            this.LoadAsync().Coroutine();
        }

        private async Task LoadAsync()
        {
            ResourcesManager.Instance.SetLoader(new UnityResourcesManager());
            SceneResManager.Instance.SetLoader(new UnitySceneLoader());
            TimeInfo.Instance.Init();
            TypesManager.Instance.Init();
            ObjectPool.Instance.Init();
            GameObjectPool.Instance.Init();

            //加载资源前要先创建这两个
            ObjectFactory.Create<ResourcesRefDetection>();
            ObjectFactory.Create<TaskManager>();

            //加载配置表，配置表是资源，所以放在这里
            var configMgr = ObjectFactory.Create<ConfigManager>();
            using var configList = XList<Task>.Create();
            await configMgr.DeserializeConfigs(configList);
            await Task.WhenAll(configList);     //等待配置表加载完毕才能执行下面的内容
            
            ObjectFactory.Create<TimerManager>();
            ObjectFactory.Create<MiniTweenManager>();
            ObjectFactory.Create<Global>();
            ObjectFactory.Create<UIEventManager>();
            ObjectFactory.Create<UIManager>();

            var sceneController = ObjectFactory.Create<SceneController>();
            sceneController.LoadSceneAsync<StartScene>(SceneName.Start);
        }

        public void Update()
        {
            try
            {
                ThreadSynchronizationContext.Instance.Update();
                Common.Instance.Update();
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        public void LateUpdate()
        {
            try
            {
                Common.Instance.LateUpdate();
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        public void FixedUpdate()
        {
            try
            {
                Common.Instance.FixedUpdate();
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        public void Dispose()
        {
            EventManager.Instance.Dispose();
            TypesManager.Instance.Dispose();
            Common.Instance.Dispose();
            ObjectPool.Instance.Dispose();
            GameObjectPool.Instance.Dispose();
            TimeInfo.Instance.Dispose();
            ResourcesManager.Instance.Dispose();

            Instance = null;
        }
    }
}
