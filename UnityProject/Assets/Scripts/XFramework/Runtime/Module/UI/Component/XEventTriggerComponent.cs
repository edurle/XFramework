﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace XFramework
{
    public class XEventTriggerComponent : UI.Component, IAwake<XEventTrigger>
    {
        private XEventTrigger et;

        public void Initialize(XEventTrigger eventTrigger)
        {
            this.et = eventTrigger;
        }

        protected override void OnDestroy()
        {
            this.RemoveAllListeners();
            this.et = null;
            base.OnDestroy();
        }

        public XEventTrigger Get()
        {
            return this.et;
        }

        public void AddListener(EventTriggerType triggerType, UnityAction<PointerEventData> action)
        {
            this.Get().AddListener(triggerType, action);
        }

        public void RemoveListener(EventTriggerType triggerType, UnityAction<PointerEventData> action)
        {
            this.Get().RemoveListener(triggerType, action);
        }

        public void RemoveListener(EventTriggerType triggerType)
        {
            this.Get().RemoveListener(triggerType);
        }

        public void RemoveAllListeners()
        {
            this.Get().RemoveAllListeners();
        }
    }

    public static class EventTriggerExtensions
    {
        public static XEventTriggerComponent GetXEventTrigger(this UI self)
        {
            XEventTriggerComponent comp = self.GetUIComponent<XEventTriggerComponent>();
            if (comp != null)
                return comp;

            var et = self.GetComponent<XEventTrigger>();
            if (!et)
                return null;

            comp = ObjectFactory.Create<XEventTriggerComponent, XEventTrigger>(et, true);
            self.AddUIComponent(comp);

            return comp;
        }

        public static XEventTriggerComponent GetXEventTrigger(this UI self, string key)
        {
            UI ui = self.GetFromKeyOrPath(key);
            return ui?.GetXEventTrigger();
        }
    }
}
