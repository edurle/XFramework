﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.EventSystems;

namespace XFramework
{
    public interface IUIBehaviour
    {
        UIBehaviour UIBehaviour { get; }
    }

    public abstract class UIComponent<T> : UI.Component, IAwake<T>, IUIBehaviour where T : UIBehaviour
    {
        protected T uiBehaviour;

        UIBehaviour IUIBehaviour.UIBehaviour => this.uiBehaviour;

        public RectTransformComponent RectTransform => parent?.GetRectTransform();

        public virtual void Initialize(T uiBehaviour)
        {
            this.uiBehaviour = uiBehaviour;
        }

        public T Get() => this.uiBehaviour as T;

        protected sealed override void OnDestroy()
        {
            this.Destroy();
            this.uiBehaviour = null;
            base.OnDestroy();
        }

        protected virtual void Destroy()
        {

        }
    }
}
