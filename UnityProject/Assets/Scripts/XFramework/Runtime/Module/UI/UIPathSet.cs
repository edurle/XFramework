using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XFramework
{
    public static class UIPathSet
    {
        #region 自动生成区

        public const string UIStart = "UI/Prefabs/Start/StartPanel";

		public const string UISetting = "UI/Prefabs/Setting/SettingPanel";

		public const string UIMain = "UI/Prefabs/Main/MainPanel";

		public const string UILoading = "UI/Prefabs/Loading/Loading";

		public const string UICharacter = "UI/Prefabs/Character/CharacterPanel";

		public const string UIThingItem = "UI/Prefabs/Character/UIThingItem";

		public const string UIEquipItem = "UI/Prefabs/Character/UIEquipItem";

		public const string UIHRD = "UI/Prefabs/HRD/UIHRD";

		public const string UIHRDItem = "UI/Prefabs/HRD/UIHRDItem";

        #endregion 1

        #region 手动添加区



        #endregion 2
    }
}
