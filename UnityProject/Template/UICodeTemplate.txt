using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace XFramework
{
	[UIEvent(UIType.(UITypeName))]
    internal sealed class (className)Event : AUIEvent
    {
	    public override string Key => UIPathSet.(UITypeName);

        public override bool IsFromPool => true;

        public override UI OnCreate()
        {
            return UI.Create<(className)>();
        }
    }

    public partial class (className) : UI, IAwake
	{	
		public void Initialize()
		{
			
		}
		
		protected override void OnClose()
		{
			base.OnClose();
		}
	}
}
